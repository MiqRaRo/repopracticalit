import { LitElement, html } from 'lit-element';

class PersonaFooter extends LitElement {

  static get properties(){
    return{};
  }

  constructor() {
    super();
  }

  updated(changedProperties) {}

  render (){
      return html`
        <h5>© PersonaApp 2021</h5>
      `;
  }

}

customElements.define('persona-footer', PersonaFooter);
