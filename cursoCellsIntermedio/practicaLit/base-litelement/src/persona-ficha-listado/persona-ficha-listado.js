import { LitElement, html } from 'lit-element';

class PersonaFichaListado extends LitElement {

  static get properties(){
    return{
      personId : {type: Number},
      fname : {type : String},
      yearsInCompany : {type: Number},
      profile : {type : String},
      photo: {type : Object}
    };
  }

  constructor() {
    super();
  }

  updated(changedProperties) {}

  render (){
      return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

        <div class="card h-100">
          <img src="${this.photo.src}" class="card-img-top" alt="${this.photo.alt}" height="300px" width="300px">
          <div class="card-body">
            <h5 class="card-title">${this.fname}</h5>
            <p class="card-text">${this.profile}</p>
            <ul class="list-group list-group-flush">
              <li class="list-group-items">${this.yearsInCompany} Años en la empresa</li>
            </ul>
          </div>
          <div class="card-footer">
            <button class="btn btn-danger col-5" @click="${this.deleteEvent}"><strong>X</strong></button>
            <button class="btn btn-info col-5 offset-1" @click="${this.moreInfo}"><strong>Info</strong></button>
          </div>
        </div>
      `;
  }

  deleteEvent(e) {
    console.log("deleteEvent");
    console.log(e);

    this.dispatchEvent(
      new CustomEvent(
        "delete-event",
        {
          "detail": {
            "personId": this.personId
          }
        }
      )
    );
  }

  moreInfo(e){
    console.log("moreInfo");
    console.log("Se ha pedido mas información de la persona " + this.fname + " de indice " + this.personId);

    this.dispatchEvent(
      new CustomEvent(
        "info-person",
        {
          "detail": {
            "personId": this.personId
          }
        }
      )
    );
  }

}

customElements.define('persona-ficha-listado', PersonaFichaListado);
