import { LitElement, html } from 'lit-element';
import '../emisor-evento/emisor-evento.js';

class TestApi extends LitElement {

  static get properties(){
      return {
        movies : {type: Array}
      };
  }

  constructor() {
    super();

    this.movies = [];
    this.getMovieData();
  }

  render (){
      return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

        <h3>Test API</h3>
        ${this.movies.map(
          (movie, index) => html`
            <div>
              La película es ${movie.title} fue dirigida por ${movie.director}
            </div>
          `
        )}
      `;
  }

  getMovieData() {
    console.log("ini getMovieData");
    console.log("Obteniendo datos de las peliculas");

    let xhr = new XMLHttpRequest();

    xhr.onload = () => {
      if(xhr.status === 200){
        console.log("Petición completada correctamente");
        console.log(xhr);

        let APIResponse = JSON.parse(xhr.responseText);

        console.log(APIResponse);

        this.movies = APIResponse.results;

      }
    };

    xhr.open("GET", "https://swapi.dev/api/films/");
    xhr.send();

    console.log("fin getMovieData");

  }

}

customElements.define('test-api', TestApi);
