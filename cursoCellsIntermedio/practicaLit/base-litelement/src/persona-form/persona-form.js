import { LitElement, html } from 'lit-element';

class PersonaForm extends LitElement {

  static get properties(){
    return{
      editingPerson : {type: Boolean},
      personId : {type: Number},
      person: {type: Object}
    };
  }

  constructor() {
    super();

    this.resetFormData();
  }

  updated(changedProperties) {
    //console.log("updated properties persona-form");

    if(changedProperties.has("person")){
      //console.log("Ha cambiado el valor de la propiedad person en persona-form");
    }
    if(changedProperties.has("personId")){
      //console.log("Ha cambiado el valor de la propiedad personId en persona-form");
      if(this.personId === -1){
        this.editingPerson = false;
      }else{
        this.editingPerson = true;
      }
    }
  }
  // ?disabled=${this.editingPerson}
  render (){
      return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <div>
          <form>
            <div class="form-group">
              <label>Nombre completo</label>
              <input type="text" class="form-control" placeholder="Nombre completo" @input="${this.updateNombre}" .value="${this.person.name}" ?disabled="${this.editingPerson}"/>
            </div>
            <div class="form-group">
              <label>Perfil</label>
              <textarea type="text" class="form-control" placeholder="Perfil" rows="5" @input="${this.updatePerfil}" .value="${this.person.profile}"></textarea>
            </div>
            <div class="form-group">
              <label>Años en la empresa</label>
              <input type="text" class="form-control" placeholder="Años en la empresa" @input="${this.updateAnosEnLaEmpresa}" .value="${this.person.yearsInCompany}"/>
            </div>
            <button class="btn btn-primary" @click="${this.goBack}"><strong>Atrás</strong></button>
            <button class="btn btn-success" @click="${this.storePerson}"><strong>Guardar</strong></button>
          </form>
        </div>
      `;
  }

  resetFormData(){
    this.editingPerson = false;
    this.personId = -1;
    this.person = {};
    this.person.name = "";
    this.person.profile = "";
    this.person.yearsInCompany = "";
  }

  updateNombre(e){
    this.person.name = e.target.value;
    this.person.photo = {};
    this.person.photo.src = "";
    this.person.photo.alt = e.target.value;
  }

  updatePerfil(e){
    this.person.profile = e.target.value;
  }

  updateAnosEnLaEmpresa(e){
    this.person.yearsInCompany = e.target.value;
  }

  goBack(e){
    console.log("goBack");
    e.preventDefault();

    this.dispatchEvent(
      new CustomEvent("persona-form-close",{}
      )
    );

    this.resetFormData();
  }

  storePerson(e){
    console.log("storePerson");
    e.preventDefault();

    if(this.personId === -1){
      this.dispatchEvent(
        new CustomEvent(
          "persona-form-store",{
            detail: {
              person: {
                name: this.person.name,
                profile: this.person.profile,
                yearsInCompany: this.person.yearsInCompany,
                photo: this.person.photo
              }
            }
          }
        )
      );
    }else{
      this.dispatchEvent(
        new CustomEvent(
          "persona-form-edit",{
            detail: {
              personId : this.personId,
              person: {
                name: this.person.name,
                profile: this.person.profile,
                yearsInCompany: this.person.yearsInCompany,
                photo: this.person.photo
              }
            }
          }
        )
      );
    }

    this.resetFormData();
  }

}

customElements.define('persona-form', PersonaForm);
