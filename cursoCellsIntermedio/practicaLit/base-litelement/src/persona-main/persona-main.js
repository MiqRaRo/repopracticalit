import { LitElement, html } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';

class PersonaMain extends LitElement {

  static get properties(){
    return{
      people : {type: Array},
      showPersonForm : {type: Boolean},
      filters : {type: Object},
    };
  }

  constructor() {
    super();
    this.people = [
      {
        name : "Monkey D. Luffy",
        yearsInCompany : 10,
        profile : "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
        photo: {
          src: "./img/img.jpg",
          alt: '"Monkey D. Luffy"'
        }
      },
      {
        name : "Roronoa Zoro",
        yearsInCompany : 9,
        profile : "Sed iaculis quis mi id feugiat",
        photo: {
          src: "./img/img2.jpg",
          alt: "Roronoa Zoro"
        }
      },
      {
        name : "Nami",
        yearsInCompany : 8,
        profile : "Nam erat ex, rutrum et leo id, dignissim ultrices lorem",
        photo: {
          src: "./img/img3.jpg",
          alt: "Nami"
        }
      },
      {
        name : "Vinsmoke Sanji",
        yearsInCompany : 8,
        profile : "Curabitur elementum faucibus sodales",
        photo: {
          src: "./img/img4.jpg",
          alt: "Nami"
        }
      },
      {
        name : "Nico Robin",
        yearsInCompany : 4,
        profile : "Fusce viverra et odio nec venenatis",
        photo: {
          src: "./img/img5.jpg",
          alt: "Nami"
        }
      }

    ];

    this.showPersonForm = false;

    this.filters = {};
    this.filters.yearsInCompany;
  }

  updated(changedProperties) {
    console.log("updated");

    if(changedProperties.has("showPersonForm")){
      console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");
      if(this.showPersonForm === true){
        this.showPersonFormData();
      }else{
        this.showPersonList();
      }
    }

    if(changedProperties.has("people")){
      console.log("Ha cambiado el valor de la propiedad people en persona-main, tamaño " + this.people.length);
      this.dispatchEvent(
        new CustomEvent(
          "updated-people",{
            detail: {
              people: this.people
            }
          }
        )
      );
    }

    if(changedProperties.has("filters")){
      console.log("Ha cambiado el valor de la propiedad filters en persona-main");

      //this.requestUpdate();
    }
  }

  storePerson(e){
    console.log("storePerson");

    //var tmpPeople = Array.from(this.people);
    //tmpPeople.push(e.detail.person);
    //this.people = tmpPeople;

    this.people = [...this.people, e.detail.person];

    console.log(JSON.stringify(e.detail.person));

    this.showPersonForm = !this.showPersonForm;
  }

  editPerson(e){
    console.log("editPerson");
    console.log("Modificamos la persona en el indice " + e.detail.personId);
    console.log(JSON.stringify(e.detail.person));

    //this.people[e.detail.personId] = e.detail.person;

    this.people = this.people.map(
      (item, index) => index == e.detail.personId
        ? item = e.detail.person : item
    );

    console.log(JSON.stringify(e.detail.person));

    this.showPersonForm = !this.showPersonForm;
  }

  personFormClose(){
    console.log("personFormClose");
    console.log("Se ha cerrado el formulario de la persona");
    this.showPersonForm = !this.showPersonForm;
  }

  showPersonList(){
    console.log("showPersonList");
    console.log("Mostrando el listado de personas");
    this.shadowRoot.getElementById("personForm").classList.add("d-none");
    this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
  }

  showPersonFormData(){
    console.log("showPersonFormData");
    console.log("Mostrando el formulario de persona");
    this.shadowRoot.getElementById("personForm").classList.remove("d-none");
    this.shadowRoot.getElementById("peopleList").classList.add("d-none");
  }

  render (){
      return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <h1 class="text-center">Personas</h1>
        <div class="row" id="peopleList">
          <div class="row row-cols-1 row-cols-sm-2 row-cols-lg-3 row-cols-lg-4">
            ${this.people.filter(
              person => parseInt(person.yearsInCompany) <= parseInt(this.filters.yearsInCompany)
            ).map(
            (person, index) => html`
            <persona-ficha-listado
            fname="${person.name}"
            yearsInCompany="${person.yearsInCompany}"
            .photo="${person.photo}"
            profile="${person.profile}"
            personId="${index}"
            @delete-event="${this.processDeleteEvent}"
            @info-person="${this.processInfoEvent}"
            ></persona-ficha-listado>
            `
          )}
          </div>
        </div>
        <div class="row">
              <persona-form
               id="personForm"
               class="d-none border rounded border-primary"
               @persona-form-close="${this.personFormClose}"
               @persona-form-store="${this.storePerson}"
               @persona-form-edit="${this.editPerson}"></persona-form>
        </div>
      `;
  }

  processDeleteEvent(e){
    console.log("processEvent");
    console.log(e);

    this.people = this.people.filter((item, index) => {
      return index !== e.detail.personId;
    });
  }

  processInfoEvent(e){
    console.log("processInfoEvent");
    console.log(e);

    console.log("Se recibe el Indice " + e.detail.personId + " correspondiente a la persona " + JSON.stringify(this.people[e.detail.personId]));

    this.showPersonForm = true;

    this.shadowRoot.getElementById("personForm").person = this.people[e.detail.personId];
    this.shadowRoot.getElementById("personForm").personId = e.detail.personId;

  }

}

customElements.define('persona-main', PersonaMain);
