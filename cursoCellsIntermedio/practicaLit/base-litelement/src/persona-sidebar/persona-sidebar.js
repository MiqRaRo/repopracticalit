import { LitElement, html } from 'lit-element';

class PersonaSidebar extends LitElement {

  static get properties(){
    return{
      peopleStats: {type:Object},
      filters : {type:Object},
      maxYearsInCompany : {type:Number}
    };
  }

  constructor() {
    super();

    this.peopleStats = {};
    this.filters = {};
    this.filters.yearsInCompany = 10;
    this.maxYearsInCompany = 10;
  }

  updated(changedProperties) {
    if(changedProperties.has("filters")){
      console.log("Ha cambiado la propiedad filters en persona-sidebar");

      this.dispatchEvent(
        new CustomEvent(
          "filters-changed",
          { detail : this.filters}
        )
      );
    }
  }

  render(){
      return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <aside>
          <section>
            <div class="mt-5">
              Hay <span class="badge bg-pill bg-primary">${this.peopleStats.numberOfPeople}</span>
            </div>
            <div class="mt-5">
              <button
               class="w-100 btn bg-success"
               style="font-size: 50px"
               @click="${this.newPerson}">
              <strong>+</strong></button>
            </div>
            <div class="mt-5">
              <label for="filtroAnos">Años en la empresa (Entre 0 y ${this.maxYearsInCompany}):</label>
              <input type="range" id="filtroAnos" name="filtroAnos" min="0" max="${this.maxYearsInCompany}" value="10" @change="${this.filterAnos}"> (${this.filters.yearsInCompany})
            </div>
          </section>
        </aside>
      `;
  }

  newPerson(e){
    console.log("newPerson en pesona-sidebar");
    console.log("se va a crear una persona");

    this.dispatchEvent(
      new CustomEvent(
        "new-person",
        {}
      )
    );
  }

  limpiarFiltros(e){
    this.filters = {};
  }

  filterAnos(e){
    console.log("filterAnos");
    console.log("Valor del filtro " + e.target.value);

    this.filters = { ...this.filters, yearsInCompany : e.target.value};
  }

}

customElements.define('persona-sidebar', PersonaSidebar);
