import { LitElement, html } from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';

class PersonaApp extends LitElement {

  static get properties(){
    return{
      people : {type:Array}
    };
  }

  constructor() {
    super();
  }

  updated(changedProperties) {
    if(changedProperties.has("people")){
      console.log("Ha cambiado la propiedad people en persona-app");
      this.shadowRoot.querySelector("persona-stats").people = this.people;
      var maxYearsInCompany = Math.max.apply(Math, this.people.map(function(o) { return o.yearsInCompany; }));
      this.shadowRoot.querySelector("persona-sidebar").maxYearsInCompany = maxYearsInCompany;
    }
  }

  render (){
      return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <persona-header></persona-header>
        <div class="row">
          <persona-sidebar class="col-2" @new-person="${this.newPerson}" @filters-changed="${this.filterChanged}"></persona-sidebar>
          <persona-main class="col-10" id="personaMain" @updated-people="${this.updatedPeople}"></persona-main>
        </div>
        <persona-footer></persona-footer>
        <persona-stats @updated-people-stats="${this.updatedPeopleStats}"></persona-stats>
      `;
  }

  filterChanged(e){
    console.log("filterChanged en persona-app");

    var filters = {yearsInCompany : e.detail.yearsInCompany};

    this.shadowRoot.querySelector("persona-main").filters = filters;
  }

  newPerson(e){
    console.log("newPerson en persona-app");

    this.shadowRoot.querySelector("persona-main").showPersonForm = !(this.shadowRoot.querySelector("persona-main").showPersonForm);
  }

  updatedPeople(e){
    console.log("updatedPeople en persona-app");
    console.log("people size en persona-app " + e.detail.people.length);

    this.people = e.detail.people;
  }

  updatedPeopleStats(e){
    console.log("updatedPeopleStats");

    this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;
  }

}

customElements.define('persona-app', PersonaApp);
