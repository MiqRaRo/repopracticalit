import { LitElement, html } from 'lit-element';

class PersonaHeader extends LitElement {

  static get properties(){
    return{};
  }

  constructor() {
    super();
  }

  updated(changedProperties) {}

  render (){
      return html`
        <h1>PersonaHeader</h1>
      `;
  }

}

customElements.define('persona-header', PersonaHeader);
